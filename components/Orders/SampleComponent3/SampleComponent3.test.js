/**
 * @jest-environment jsdom
 */

import React from "react";
import { render, screen } from "@testing-library/react";
import SampleComponent3 from "./SampleComponent3";

describe("SampleComponent3", () => {
    it("should render without crashing", () => {
        try {
            render(<SampleComponent3 />);
            expect(true).toBe(true);
        } catch (error) {
            expect(false).toBe(true);
        }
    });
});
