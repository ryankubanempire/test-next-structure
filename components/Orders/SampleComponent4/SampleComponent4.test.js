/**
 * @jest-environment jsdom
 */

import React from "react";
import { render, screen } from "@testing-library/react";
import SampleComponent4 from "./SampleComponent4";

describe("SampleComponent4", () => {
    it("should render without crashing", () => {
        try {
            render(<SampleComponent4 />);
            expect(true).toBe(true);
        } catch (error) {
            expect(false).toBe(true);
        }
    });
});
