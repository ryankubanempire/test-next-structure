/**
 * @jest-environment jsdom
 */

import React from "react";
import { render, screen } from "@testing-library/react";
import SampleComponent1 from "./SampleComponent1";

describe("SampleComponent1", () => {
    it("should render without crashing", () => {
        try {
            render(<SampleComponent1 />);
            expect(true).toBe(true);
        } catch (error) {
            expect(false).toBe(true);
        }
    });
});
