/**
 * @jest-environment jsdom
 */

import React from "react";
import { render, screen } from "@testing-library/react";
import SampleComponent2 from "./SampleComponent2";

describe("SampleComponent2", () => {
    it("should render without crashing", () => {
        try {
            render(<SampleComponent2 />);
            expect(true).toBe(true);
        } catch (error) {
            expect(false).toBe(true);
        }
    });
});
