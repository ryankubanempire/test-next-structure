import axios from "axios";

class CustomAxiosError {
	static CANCELLATION_MESSAGE = "Request Cancelled";
	static UNKNOWN_ERROR_MESSAGE =
		"Something went wrong. Please try again later.";

	constructor(object) {
		if (axios.isCancel(object)) {
			this.status = 499;
			this.message = CustomAxiosError.CANCELLATION_MESSAGE;
		} else if (object.response) {
			this.status = object.response.status;
			this.message = object.response.data.message;
		} else if (object.request) {
			this.status = object.request.status;
			this.message = CustomAxiosError.UNKNOWN_ERROR_MESSAGE;
		} else if (object.message) {
			this.status = 0;
			this.message = object.message;
		} else {
			this.status = 0;
			this.message = CustomAxiosError.UNKNOWN_ERROR_MESSAGE;
		}
	}

	static isCancel() {
		if (this.message === CustomAxiosError.CANCELLATION_MESSAGE) {
			return true;
		}

		return false;
	}
}

export default CustomAxiosError;
