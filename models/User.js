class User {
	constructor(object) {
		this.id = object.id; // If ID is in the output. Better if not.
		this.email = object.email;
		this.name = object.name;
		this.supplierId = object.supplier_id;
		this.createdAt = new Date(object.created_at);
		this.updatedAt = new Date(object.updated_at);
	}
}

export default User;
