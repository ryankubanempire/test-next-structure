import { login } from "../redux/actions/authentication.action";
import { useDispatch, useSelector } from "react-redux";
import { useEffect } from "react";
import { useRouter } from "next/dist/client/router";
import { useSession, getSession } from "next-auth/client";
import { wrapper } from "../redux/store";
import Link from "next/link";
import { authenticationActions } from "../redux/slices/authentication.slice";
import { routesActions } from "../redux/slices/routes.slice";
import styles from "../styles/index.module.css";

const IndexPage = (props) => {
    // const authentication = useSelector((state) => state.authentication);
    // console.log(props);
    // console.log(authentication);

    // As this page uses Server Side Rendering, the `session` will be already
    // populated on render without needing to go through a loading stage.
    // This is possible because of the shared context configured in `_app.js` that
    // is used by `useSession()`.

    const [session, loading] = useSession();

    console.log("Session: ", session);
    console.log("Loading: ", loading);

    return (
        <div>
            <h1>Index Page</h1>
            <Link href="/profile">Profile</Link>
            <span>&nbsp;|&nbsp;</span>
            <Link href="/suppliers">Suppliers</Link>
        </div>
    );
};

export const getServerSideProps = wrapper.getServerSideProps((store) => async (context) => {
    // const props = {};
    // const data = await store.dispatch(login("email@email.com", "password-here"));
    // props.data = "asd";
    // return { props };

    store.dispatch(routesActions.saveRoute("index"));

    const session = await getSession(context);
    // store.dispatch(authenticationActions.replace(session.user));

    if (!session) {
        return {
            redirect: {
                destination: "/api/auth/signin",
                permanent: false
            }
        };
    }

    return {
        props: {
            session: session
        }
    };
});

export default IndexPage;
