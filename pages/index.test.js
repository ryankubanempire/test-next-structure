/**
 * @jest-environment jsdom
 */

import { Provider } from "react-redux";
import { render, screen } from "@testing-library/react";
import IndexPage from "./index";
import React from "react";
import store, { wrapper } from "../redux/store";

describe("IndexPage", () => {
    it("should render without crashing", () => {
        try {
            render(
                <Provider store={store}>
                    <IndexPage />
                </Provider>
            );
            expect(true).toBe(true);
        } catch (error) {
            expect(false).toBe(true);
        }
    });
});
