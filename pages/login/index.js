import styles from "./index.module.css";
import Link from "next/link";
import { useSelector } from "react-redux";

const LoginPage = (props) => {
    console.log(props);

    const authentication = useSelector((state) => state.authentication);

    console.log(authentication);

    return (
        <div>
            <h1>Login Page</h1>
            <Link href="/">Index</Link>
            <span>&nbsp;</span>
            <Link href="/profile">Profile</Link>
        </div>
    );
};

export const getServerSideProps = async (context) => {
    console.log("login / getServerSideProps");

    const props = {};

    props.renderer = "login/server";

    return { props };
};

export default LoginPage;
