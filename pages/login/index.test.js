/**
 * @jest-environment jsdom
 */

import { Provider } from "react-redux";
import { render, screen } from "@testing-library/react";
import LoginPage, { getServerSideProps } from "./index";
import React from "react";
import store, { wrapper } from "../../redux/store";

it("should render without crashing", () => {
    try {
        render(
            <Provider store={store}>
                <LoginPage />
            </Provider>
        );
        expect(true).toBe(true);
    } catch (error) {
        expect(false).toBe(true);
    }
});

it("should render props from the server correctly", () => {
    const value = getServerSideProps();

    expect(value).toEqual(
        Promise.resolve({
            props: {
                renderer: "login/server"
            }
        })
    );
});
