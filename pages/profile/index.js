import { getSession } from "next-auth/client";
import { routesActions } from "../../redux/slices/routes.slice";
import { wrapper } from "../../redux/store";
import Link from "next/link";
import styles from "./index.module.css";

const ProfilePage = () => {
    return (
        <div>
            <h1>Profile Page</h1>
            <Link href="/">Index</Link>
            <span>&nbsp;|&nbsp;</span>
            <Link href="/suppliers">Suppliers</Link>
        </div>
    );
};

export const getServerSideProps = wrapper.getServerSideProps((store) => async (context) => {
    const session = await getSession(context);
    store.dispatch(routesActions.saveRoute("profile"));

    if (!session) {
        return {
            redirect: {
                destination: "/api/auth/signin",
                permanent: false
            }
        };
    }

    return {
        props: {
            session: session
        }
    };
});

export default ProfilePage;
