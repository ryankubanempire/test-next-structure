/**
 * @jest-environment jsdom
 */

import { Provider } from "react-redux";
import { render, screen } from "@testing-library/react";
import ProfilePage, { getServerSideProps } from "./index";
import React from "react";
import store, { wrapper } from "../../redux/store";

it("should render without crashing", () => {
    try {
        render(
            <Provider store={store}>
                <ProfilePage />
            </Provider>
        );
        expect(true).toBe(true);
    } catch (error) {
        expect(false).toBe(true);
    }
});

it("should render props from the server correctly", () => {
    const value = getServerSideProps();

    expect(value).toEqual(
        Promise.resolve({
            props: {
                renderer: "profile/server"
            }
        })
    );
});
