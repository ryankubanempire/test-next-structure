import styles from "./index.module.css";
import { useRouter } from "next/router";

const EditSupplierPage = () => {
	const router = useRouter();
	const { id } = router.query;

	return <h1>Edit Supplier {id}</h1>;
};

export default EditSupplierPage;
