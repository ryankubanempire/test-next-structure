/**
 * @jest-environment jsdom
 */

import { Provider } from "react-redux";
import { render, screen } from "@testing-library/react";
import EditSupplierPage from "./index";
import React from "react";
import store, { wrapper } from "../../../../redux/store";

jest.mock("next/router", () => ({
    useRouter: () => {
        return {
            route: "/suppliers/[id]/edit",
            pathname: "/suppliers/[id]/edit",
            query: { id: 1 },
            asPath: "/suppliers/1/edit"
        };
    }
}));

describe("EditSupplierPage", () => {
    it("should render without crashing", () => {
        try {
            render(
                <Provider store={store}>
                    <EditSupplierPage />
                </Provider>
            );
            expect(true).toBe(true);
        } catch (error) {
            expect(false).toBe(true);
        }
    });
});
