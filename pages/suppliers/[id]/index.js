import styles from "./index.module.css";
import { useRouter } from "next/router";

const SupplierDetailsPage = () => {
	const router = useRouter();
	const { id } = router.query;

	return <h1>Supplier Details {id}</h1>;
};

export default SupplierDetailsPage;
