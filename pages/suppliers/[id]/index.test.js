/**
 * @jest-environment jsdom
 */

import { Provider } from "react-redux";
import { render, screen } from "@testing-library/react";
import SupplierDetailsPage from "./index";
import React from "react";
import store, { wrapper } from "../../../redux/store";

jest.mock("next/router", () => ({
    useRouter: () => {
        return {
            route: "/suppliers/[id]",
            pathname: "/suppliers/[id]",
            query: { id: 1 },
            asPath: "/suppliers/1"
        };
    }
}));

describe("SupplierDetailsPage", () => {
    it("should render without crashing", () => {
        try {
            render(
                <Provider store={store}>
                    <SupplierDetailsPage />
                </Provider>
            );
            expect(true).toBe(true);
        } catch (error) {
            expect(false).toBe(true);
        }
    });
});
