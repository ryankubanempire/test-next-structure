import { getSession } from "next-auth/client";
import { routesActions } from "../../redux/slices/routes.slice";
import { wrapper } from "../../redux/store";
import Link from "next/link";
import styles from "./index.module.css";

const SuppliersPage = () => {
    return (
        <div>
            <h1>Suppliers Page</h1>
            <Link href="/profile">Profile</Link>
            <span>&nbsp;|&nbsp;</span>
            <Link href="/">Index</Link>
        </div>
    );
};

export const getServerSideProps = wrapper.getServerSideProps((store) => async (context) => {
    const session = await getSession(context);
    store.dispatch(routesActions.saveRoute("suppliers"));

    if (!session) {
        return {
            redirect: {
                destination: "/api/auth/signin",
                permanent: false
            }
        };
    }

    return {
        props: {
            session: session
        }
    };
});

export default SuppliersPage;
