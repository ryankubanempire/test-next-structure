/**
 * @jest-environment jsdom
 */

import { Provider } from "react-redux";
import { render, screen } from "@testing-library/react";
import SuppliersPage from "./index";
import React from "react";
import store, { wrapper } from "../../redux/store";

describe("SuppliersPage", () => {
    it("should render without crashing", () => {
        try {
            render(
                <Provider store={store}>
                    <SuppliersPage />
                </Provider>
            );
            expect(true).toBe(true);
        } catch (error) {
            expect(false).toBe(true);
        }
    });
});
