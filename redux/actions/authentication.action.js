import axios from "../../services/AxiosService";
import { authenticationActions } from "../slices/authentication.slice";
import User from "../../models/User";

export const getUser = () => {
	return async (dispatch) => {
		const response = await axios.get("auth/user");

		return new User(response.data);
	};
};

export const login = (email, password) => {
	return async (dispatch) => {
		// const response = await axios.post("auth/login", {
		// 	email: email,
		// 	password: password,
		// });

		const data = await new Promise((resolve, reject) => {
			resolve({
				user: {
					id: 0,
					name: "Jan Ryan Felipe",
					email: "ryan@kubanempire.com",
					supplier_id: null,
					created_at: new Date(),
					updated_at: new Date(),
				},
				access_token: "generated-access-token",
			});
		});
		const response = { data };

		const user = new User(response.data.user);
		const accessToken = response.data.access_token;

		dispatch(
			authenticationActions.saveLogin({
				name: user.name,
				email: user.email,
				supplierId: user.supplierId,
			})
		);

		// Save to cookie here

		return {
			user: user,
			accessToken: accessToken,
		};
	};
};

export const logout = () => {
	return async (dispatch) => {
		const response = await axios.post("auth/logout");

		dispatch(authenticationActions.saveLogout());

		return true;
	};
};
