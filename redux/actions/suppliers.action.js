import axios from "../../services/AxiosService";
import { suppliersActions } from "../slices/suppliers.slice";
import Supplier from "../../models/Supplier";

export const getAll = () => {
	return async (dispatch) => {
		const response = await axios.get("suppliers");
		const suppliers = response.data.map((datum) => {
			return new Supplier(datum);
		});

		dispatch(suppliersActions.fill(suppliers));

		return suppliers;
	};
};

export const get = (id) => {
	return async (dispatch) => {
		const response = await axios.get(`suppliers/${id}`);

		return new Supplier(response.data);
	};
};

// Add functions like create, update and more here...
