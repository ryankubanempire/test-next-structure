import { createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

const initialState = {
    name: "",
    email: "",
    supplierId: null,
    isLoggedIn: false
};
const authenticationSlice = createSlice({
    name: "authentication",
    initialState: initialState,
    reducers: {
        saveLogin: (state, action) => {
            const user = action.payload;

            return {
                ...state,
                name: user.name,
                email: user.email,
                supplierId: user.supplierId,
                isLoggedIn: true
            };

            // You can also update your state like you're mutating it here...
            // ReduxJS Toolkit takes inspiration from Immer and Autodux to let you write "mutative" immutable update logic
        },
        saveLogout: (state, action) => {
            return {
                ...initialState
            };

            // Update your state like you're mutating it here...
            // ReduxJS Toolkit takes inspiration from Immer and Autodux to let you write "mutative" immutable update logic
        },
        replace: (state, action) => {
            const user = action.payload;
            if (!user) {
                return {
                    ...initialState
                };
            }

            return {
                ...state,
                name: user.name,
                email: user.email,
                isLoggedIn: !!action.payload
            };
        }
        // More reducers here...
    },
    extraReducers: {
        [HYDRATE]: (state, action) => {
            // console.log("HYDRATE", state, action.payload);
            return {
                ...state,
                ...action.payload.authentication
            };
        }
    }
});

export const authenticationActions = authenticationSlice.actions;
export default authenticationSlice;
