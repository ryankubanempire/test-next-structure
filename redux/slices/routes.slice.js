import { createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

const initialState = {
    visited: []
};
const routesSlice = createSlice({
    name: "routes",
    initialState: initialState,
    reducers: {
        saveRoute: (state, action) => {
            const name = action.payload;

            return {
                ...state,
                // visited: [...state.visited, name]
                visited: state.visited.concat(name)
            };

            // You can also update your state like you're mutating it here...
            // ReduxJS Toolkit takes inspiration from Immer and Autodux to let you write "mutative" immutable update logic
        }
        // More reducers here...
    },
    extraReducers: {
        [HYDRATE]: (state, action) => {
            // console.log("HYDRATE", state, action.payload);
            return {
                ...state,
                ...action.payload.routes
            };
        }
    }
});

export const routesActions = routesSlice.actions;
export default routesSlice;
