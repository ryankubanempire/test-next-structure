import { createSlice } from "@reduxjs/toolkit";
import { HYDRATE } from "next-redux-wrapper";

const initialState = {
    suppliers: [],
    selectedSupplier: null
};
const suppliersSlice = createSlice({
    name: "suppliers",
    initialState: initialState,
    reducers: {
        selectSupplier: (state, action) => {
            const supplier = action.payload;

            return {
                ...state,
                selectedSupplier: { ...supplier }
            };

            // You can also update your state like you're mutating it here...
            // ReduxJS Toolkit takes inspiration from Immer and Autodux to let you write "mutative" immutable update logic
        },
        add: (state, action) => {
            const supplier = action.payload;

            return {
                ...state,
                suppliers: [...state.suppliers, supplier]
            };

            // Update your state like you're mutating it here...
            // ReduxJS Toolkit takes inspiration from Immer and Autodux to let you write "mutative" immutable update logic
        },
        remove: (state, action) => {
            const id = action.payload;

            return {
                ...state,
                suppliers: state.suppliers.filter((supplier) => {
                    return supplier.id !== id;
                }),
                selectedSupplier: state.selectedSupplier.id === id ? null : { ...state.selectedSupplier }
            };
        },
        fill: (state, action) => {
            const suppliers = action.payload;

            return {
                ...state,
                suppliers: [...suppliers]
            };
        },
        clear: (state, action) => {
            const id = action.payload;

            return { ...initialState };
        }
        // More reducers here...
    },
    extraReducers: {
        [HYDRATE]: (state, action) => {
            // console.log("HYDRATE", state, action.payload);
            return {
                ...state,
                ...action.payload.suppliers
            };
        }
    }
});

export const suppliersActions = suppliersSlice.actions;
export default suppliersSlice;
