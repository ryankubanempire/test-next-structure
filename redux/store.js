import { configureStore } from "@reduxjs/toolkit";
import { createWrapper } from "next-redux-wrapper";
import authenticationSlice from "./slices/authentication.slice";
import routesSlice from "./slices/routes.slice";
import suppliersSlice from "./slices/suppliers.slice";

const store = configureStore({
    reducer: {
        authentication: authenticationSlice.reducer,
        routes: routesSlice.reducer,
        suppliers: suppliersSlice.reducer
    }
});

const makeStore = () => store;

export const wrapper = createWrapper(makeStore);

export default store;
