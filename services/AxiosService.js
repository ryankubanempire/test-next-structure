import axios from "axios";
import CustomAxiosError from "../models/CustomAxiosError";

const axiosInstance = axios.create({
    baseURL: process.env.NEXT_PUBLIC_API_BASE_URL
});

axiosInstance.interceptors.request.use((config) => {
    // Add accessToken from cookie in request Authorization header as "Bearer {accessToken}"
    // const accessToken = LocalStorageService.getAccessToken();

    // if(accessToken !== null &&
    //    accessToken !== undefined &&
    //    config.url &&
    //    API_BASE_URL &&
    //    config.url.includes(API_BASE_URL)
    // ) {
    //     if(typeof config.headers === "undefined") {
    //         config.headers = {};
    //     }

    //     config.headers.Authorization = "Bearer " + accessToken;
    // }

    return config;
});

axiosInstance.interceptors.response.use((response) => {
    return response;
}, (error) => {
    return Promise.reject(new CustomAxiosError(error));
});

export default axiosInstance;